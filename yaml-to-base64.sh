#!/bin/bash


YAML_PATH=$1

if [ -z "$YAML_PATH" ]; then
  echo "Path to yaml file is required"
  exit 1
fi

if [ ! -f "$YAML_PATH" ]; then
  echo "$YAML_PATH doesn't exist"
  exit 1
fi

cat "$YAML_PATH" | base64 -w 0

