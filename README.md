#Spring Boot - Rest con GitLab CI y Kubernetes:

-Test sobre una app Spring Boot que usa Rest (get)

-Package de la app

-Build y push de imagen en docker hub a través de un registry privado

-Deploy con la imagen anterior en gcp a través de kubernetes:

NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)        AGE
[service/sp-service     LoadBalancer   10.0.145.193   191.233.27.114   80:30544/TCP   89s]

-Con kubectl get all entrega ip para verificar deploy de la app; en este caso, nos entrego : 191.233.27.114:80/greeting

![Diagrama](https://gitlab.com/nelsonv_chile/virtual-nv/-/blob/prev/diagrama.jpg)