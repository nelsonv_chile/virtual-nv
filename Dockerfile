FROM openjdk:8u111-jdk-alpine

ADD target/springboot-rest-example-0.6.10.jar app.jar

EXPOSE 8080 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar", "app.jar"]
